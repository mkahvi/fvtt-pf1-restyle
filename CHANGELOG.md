# Change Log

## NEXT

- Chnage: Improve scrollbar definitions.

## 4.4.0

- Change: Scrollbar improvements

## 4.3.1

- Fix: Tables in actor sheet item summaries.

## 4.3.0

- New: Tables now have column borders.
- New: Tables now have more padding to not squish contents to borders.
- New: Cells with no special content no longer have insane padding.
- New: `<h3>` and `<h4>` underline in journals.

## 4.2.1

- Fix: File Browser file list being bad with large numbers of folders.
- New: Fix background image location, preventing scrolling and shifting.

## 4.2.0.0

- Handle disabled inputs better for cursor type.
- Add underline to `<h3>` and `<h4>` in chat messages.
- Set macro editor background to dark blue and foreground to light blue.
- Disable hover effect on disabled buttons.
- Restore chat notification pip bright visibility.
- 4.2.0.1: Fix for Token Action HUD button backgrounds.

## 4.1.0.0

- `<a>` links are colored a bit more readably.
- Combat controls have some extra color.
- Some core colors were adjusted to be less red and more orange.
- Distinct border for Foundry tooltips.

### Fixes

- Fix: Unidentified description rendered weird for players.

## 4.0.0.0

- Underline `<h3>` and `<h4>`.
- Clarify disabled buttons.
- Improve spellpoints layout.
- Enforce scrollbar display, removing layout jitter.
- Subskills layout improvements.
- Fix: Removed doubled border in editors.
- Item summary padding adjustments.
- Undo Foundry's `<p>` styling in item summary.
- Align template type with other type columns.
- Compress action and sheet sheet headers.

## 3.0.0.3

- Compatibility: Token Action HUD - Sub-menu buttons.

## 3.0.0.2

- Make folder names visible even when folder color is excessively bright.
- Compatibility: Token Action HUD

## 3.0.0.1

- Fix release handling.
- Prevent chat card timestamp wrap.
- Display non-default cursor for checkbox and select elements.

## 3

- Fix roll tooltip linebreak with roll values
- Unidentified items don't have their unidentified name looking like it wasn't the primary name.
- Nav background removed.
- Button circular gradient.
- Chat cards are colored based whether they're normal, blind, whisper, or self whisper.

## 2

- Nav gradients
- Table header gradients
- Skill highlight matching item highlight
- Carried total weight coloring (ported over from Little Helper as pure CSS)
- Attributes tab gradients.
- Chat card save DC button is now ugly but visible.

## 1 Initial

- All sheets
  - Rounded borders
  - TinyMCE editor has now border and background color to make it more apparent.
    - Links are darker color to make them more readable.
- All PF1 sheets
  - Less padding.
- Actor sheet
  - Quick actions are made to use less padding.
  - Header is compressed, making it all use less vertical space.
  - Item list clarifications
    - Disabled items are displayed more clearly so.
    - Item hover background color is softer gray-brown instead of blue.
- Item sheets
  - Make item sheet headers smaller.
    - Various elements are compressed.
    - Unidentified name uses smaller font.
  - Item title is colored differently.
- Chat log
  - Prettify expanded rolls and inline roll tooltips.
  - Add clickable status to basic check totals.
  - Remove clickable status on attack card fake inline roll.
