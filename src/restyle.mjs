const getDocData = (doc) => game.release.generation >= 10 ? doc : doc?.data;

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function renderChatMessage(cm, [html]) {
	// console.log(cm);
	const cmData = getDocData(cm);
	const w = cmData.whisper;
	if (w.length == 1 && w[0] == cmData.user) {
		const base = html.dataset.messageId ? html : html.closest('[data-message-id]');
		base.classList.add('self');
	}
}

Hooks.on('renderChatMessage', renderChatMessage);
